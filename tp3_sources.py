# exercice 2

def comptage_exo2(entree):
    """cette fonction prend tous les elements de la liste et compte les nombres pair dans xxx et les nombres impair dans yyy

    Args:
        nbr_p(int): est un compteur, il compte les nombres pair dans la liste
        nbr_i(int): est un compteur, il compte les nombres impair dans la liste
        zzz(int): prend les nombres de la liste un par un pour regarder s'ils sont pair ou non

    Returns:
        bool: True s'il y a plus de nombre pair False sinon
    """
    nbr_p=0
    nbr_i=0
    # au début de chaque tour de boucle
    #  xxx comptient le nombre de nombres pair dans la liste et yyy les nombres impair
    for zzz in entree:
        if zzz%2==0:
            nbr_p+=1
        else:
            nbr_i+=1
    return nbr_p>=nbr_i
def test_pairs():
    assert comptage_exo2([1,4,6,-2,-5,3,10])==True
    assert comptage_exo2([-4,5,-11,-56,5,-11])==False
    assert comptage_exo2([1,0,6,-2,0,-6,10])==True
    assert comptage_exo2([-4587558,598,-11,-56,5,-11,512,5,-6,5])==True

test_pairs()


# exercice 3

def min_sup(liste_nombres,valeur):
    """trouve le plus petit nombre d'une liste supérieur à une certaine valeur

    Args:
        liste_nombres (list): la liste de nombres
        valeur (int ou float): la valeur limite du minimum recherché

    Returns:
        int ou float: le plus petit nombre de la liste supérieur à valeur
    """
    res=None#liste_nombres[0]
    # au début de chaque tour de boucle res est le plus petit élément déjà énuméré
    # supérieur à valeur
    for elem in liste_nombres:
        if res==None:
            if elem>valeur:
                res=elem
        elif elem>valeur and elem<res:
            res=elem
    return res



def test_min_sup():
    assert min_sup([8,12,7,3,9,2,1,4,9],5)==7
    assert min_sup([-2,-5,2,9.8,-8.1,7],0)==2
    assert min_sup([5,7,6,5,7,3],10)==None
    assert min_sup([],5)==None

test_min_sup()

# exercice 4
def nb_mots ( phrase ) :
    """Fonction qui compte le nombre de mots d'une phrase

    Args:
        phrase (str): une phrase dont les mots sont séparés par des espaces (éventuellement plusieurs)

    Returns:
        int: le nombre de mots de la phrase
    """    
    resultat =0
    c1 = ' '
    # au début de chaque tour de boucle
    # c1 vaut c2
    # c2 vaut c1
    # resultat vaut 
    for c2 in phrase :
        if c1 == ' ' and c2 != ' ':
            resultat = resultat +1
        c1 = c2
    #if phrase=="":#
     #   resultat=-1#
    return resultat

#print(nb_mots(""))

def test_nb_mots():
    assert nb_mots("bonjour, il fait beau")==4
    assert nb_mots("houla!     je    mets beaucoup   d'  espaces    ")==6
    assert nb_mots("ce  test ne  marche pas ")==5
    assert nb_mots("")==0 #celui ci non plus
test_nb_mots()

#exercice5
def somme(liste):
    """
    Ecrire une fonction qui retourne la somme des nombres pairs d’une liste d’entiers.
    
    argument:
        liste: liste de nombre
        paire:nombre paires qui font partie de la liste


    returns:
        int: somme des nombres pairs

    """
    res=0
    for elem in liste:
        if elem%2==0:
            res+=elem
    return res

print(somme([3,4,5,6,7,8]))

def test_nb_pairs():
    assert somme([2,5,2,33,99])==4
    assert somme([2,8,4,6,10])==30
    assert somme([1,3,1,5,7,9,37])==0
    assert somme([])==0 
    assert somme([1,3,1,5,7,9,358])==358
test_nb_pairs()

def voyelle(mot):
    """
    Ecrire une fonction qui retourne la dernière voyelle d’une chaîne de caractères.
    Par exemple pour la chaîne "buongiorno" le résultat doit être "o" et pour la chaîne "bonjour"
    le résultat doit être "u". Si la chaîne ne contient aucune voyelle le résultat doit être None.
    
    argument:
        mots: chaine de lettre
        


    returns:
        str: derniere voyelle

    """
    res=None
    voyelle="aeiouy"
    for lettre in mot:
        if lettre in voyelle:
            res=lettre
    return res

print(voyelle("etape"))

def test_mot():
    assert voyelle("bonjour")==("u")
    assert voyelle("houla!")==("a")
    assert voyelle("qjfttfgrhhgf")==None
    assert voyelle("coucou")==("u")
test_mot()

def negatif(liste):
    """
    Ecrire une fonction qui donne la proportion de nombres strictement négatifs dans une liste

     argument:
        liste: liste de nombre
        


    returns:
        float: nombre negatif sur total de la liste
    """
    res=0
    x=len(liste)
    for elem in liste:
        if elem<0:
            res+=1
    if len(liste)==0:
        return 0
    return res/x

print(negatif([2,5,-2,-8,-2,5]))

def test_neg():
    assert negatif([4,-2,8,2,-2,-7])==0.5
    assert negatif([2,8,4,6,10])==0
    assert negatif([-8])==1
    assert negatif([])==0
    assert negatif([-2,-5,-4,8])==0.75
test_neg()
